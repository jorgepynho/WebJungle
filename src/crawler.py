#! /usr/bin/python

from bs4 import BeautifulSoup
from urlparse import urlparse

import requests
import argparse
import re

parser = argparse.ArgumentParser(description='WebJungle - web crawler')
parser.add_argument('-u','--url', help='Start URL',required=True)
args = parser.parse_args()

print
print " ".join(["Crawling:", args.url])
print

lista_urls = []

def get_page_title(soup):
	title_tag = soup.find('title')
	title = ''

	if title_tag is None:
		return ''

	for child in title_tag.children:
		title = child

	return title

def is_valid_href(href):
	if href is None:
		return False

	if href == "/#":
		return False

	if href == "#":
		return False

	if len(href) < 1:
		return False

	return True

def handle_content(url, title, content):
	return True

def get_hrefs(soup):
	h_list = []
	for link in soup.find_all('a'):
		href = link.get('href')

	return h_list

def fetch_link(url, level = 0):

	if url in lista_urls:
		return False

	if level > 3:
		return False;

	url_parts = urlparse(url)

	if len(url_parts.netloc) < 1:
		print " ".join([url, "- Error", "URL must be canonical"])
		return False

	try:
		response = requests.get(url)
	except Exception, e:
		print e
		return False

	lista_urls.append(url)

	if response.status_code != 200:
		print " ".join([url, "- Error", str(response.status_code)])
		return False

	soup = BeautifulSoup(response.content, 'html.parser')

	title = get_page_title(soup)

	handle_content(url, title, response.content)

	print " ".join([url, "(", str(level), ")"])
	print title
	print

	for link in soup.find_all('a'):
		href = link.get('href')

		if not is_valid_href(href):
			continue

		href_parts = urlparse(href)

		if len(href_parts.netloc) < 1:
			href = "".join([url_parts.scheme, "://", url_parts.netloc, href])
		elif href_parts.netloc != url_parts.netloc:
			continue

		fetch_link(href, level + 1)
		#print href

fetch_link(args.url)
